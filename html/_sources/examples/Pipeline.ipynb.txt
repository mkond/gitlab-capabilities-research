{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Example pipeline\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os \n",
    "\n",
    "import pandas as pd\n",
    "import xgboost\n",
    "\n",
    "from sklearn.model_selection import GridSearchCV\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.pipeline import Pipeline\n",
    "from sklearn.externals import joblib\n",
    "\n",
    "from ds2021_utils.data_prep.extended_df import ExtendedDataFrame\n",
    "from ds2021_utils.custom_transformers.column_caster import ColumnCaster\n",
    "from ds2021_utils.custom_transformers.dummy_transformer import DummyTransformer\n",
    "from ds2021_utils.custom_transformers.saver_transformer import SaverTransformer\n",
    "from ds2021_utils.custom_transformers.columns_remover import ColumnsRemoverTransformer\n",
    "\n",
    "from ds2021_utils.models.xgboost_learning.xgb_creator import XgbClassifierCreator\n",
    "\n",
    "DATA_PATH = \"./\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv(\"https://community.watsonanalytics.com/wp-content/uploads/2015/03/WA_Fn-UseC_-Telco-Customer-Churn.csv?cm_mc_uid=59994854347515380531105&cm_mc_sid_50200000=80103881538402088795&cm_mc_sid_52640000=47711301538402088802\")\n",
    "df = ExtendedDataFrame(df)\n",
    "df.drop_duplicates(inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['gender', 'Partner', 'Dependents', 'PhoneService', 'MultipleLines', 'InternetService', 'OnlineSecurity', 'OnlineBackup', 'DeviceProtection', 'TechSupport', 'StreamingTV', 'StreamingMovies', 'Contract', 'PaperlessBilling', 'PaymentMethod', 'SeniorCitizen']\n",
      "['SeniorCitizen', 'tenure', 'MonthlyCharges', 'TotalCharges']\n"
     ]
    }
   ],
   "source": [
    "categorical_data = df.categorical_columns + ['SeniorCitizen']\n",
    "not_categorical_data = ['customerID', 'TotalCharges', 'Churn']\n",
    "categorical_data = [c for c in categorical_data if c not in not_categorical_data]\n",
    "print(categorical_data)\n",
    "\n",
    "numeric_data = df.numeric_columns + ['TotalCharges']\n",
    "print(numeric_data)\n",
    "\n",
    "target_feature = 'Churn'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# X and Y\n",
    "X = df.copy()\n",
    "Y = df[target_feature].copy()\n",
    "Y.replace({\"Yes\": 1, \"No\": 0}, inplace=True)  # temporary broken in current pandas version (Ievgen)\n",
    "Y = Y.values.reshape(Y.shape[0], )\n",
    "\n",
    "# unique IDs\n",
    "ID = df['customerID']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# split into train and validation dataframes\n",
    "X_tr, X_ts, Y_tr, Y_ts, ID_tr, ID_ts = train_test_split(X, Y, ID, test_size=0.2, random_state=1984)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Pipeline(memory=None,\n",
       "     steps=[('cast_columns', ColumnCaster(copy=True,\n",
       "       to_categories=['gender', 'Partner', 'Dependents', 'PhoneService', 'MultipleLines', 'InternetService', 'OnlineSecurity', 'OnlineBackup', 'DeviceProtection', 'TechSupport', 'StreamingTV', 'StreamingMovies', 'Contract', 'PaperlessBilling', 'Payment...\n",
       "       reg_alpha=0, reg_lambda=1, scale_pos_weight=1, seed=None,\n",
       "       silent=True, subsample=1))])"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# construct the data preparation pipeline and save it\n",
    "pipe = Pipeline([\n",
    "    ('cast_columns', ColumnCaster(to_categories=categorical_data, to_number=numeric_data)),\n",
    "    ('save_state', SaverTransformer()),\n",
    "    ('remover', ColumnsRemoverTransformer(features_to_remove=[\"customerID\", target_feature])),\n",
    "    ('get_dummies', DummyTransformer()),\n",
    "    ('xgb_classifier', xgboost.XGBClassifier())\n",
    "])\n",
    "\n",
    "X_tr = ExtendedDataFrame(X_tr)\n",
    "pipe.fit(\n",
    "    X_tr, Y_tr, save_state__filename=\"./examples/raw_data_prepared.csv\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['./pipeline.pkl']\n"
     ]
    }
   ],
   "source": [
    "file_name = 'pipeline.pkl'\n",
    "pkl_files = joblib.dump(pipe, \"./\" + file_name)\n",
    "print(pkl_files)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## cv for pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "GridSearchCV(cv=3, error_score='raise',\n",
       "       estimator=Pipeline(memory=None,\n",
       "     steps=[('cast_columns', ColumnCaster(copy=True,\n",
       "       to_categories=['gender', 'Partner', 'Dependents', 'PhoneService', 'MultipleLines', 'InternetService', 'OnlineSecurity', 'OnlineBackup', 'DeviceProtection', 'TechSupport', 'StreamingTV', 'StreamingMovies', 'Contract', 'PaperlessBilling', 'Payment...\n",
       "       reg_alpha=0, reg_lambda=1, scale_pos_weight=1, seed=None,\n",
       "       silent=True, subsample=1))]),\n",
       "       fit_params=None, iid=True, n_jobs=1,\n",
       "       param_grid={'xgb_classifier__base_score': [0.5], 'xgb_classifier__booster': ['gbtree'], 'xgb_classifier__colsample_bylevel': [1], 'xgb_classifier__colsample_bytree': [1], 'xgb_classifier__gamma': [0], 'xgb_classifier__learning_rate': [0.1, 0.5, 0.9], 'xgb_classifier__max_delta_step': [0], 'xgb_class...[1], 'xgb_classifier__seed': [0], 'xgb_classifier__silent': [1], 'xgb_classifier__subsample': [0.7]},\n",
       "       pre_dispatch='2*n_jobs', refit=True, return_train_score='warn',\n",
       "       scoring='roc_auc', verbose=0)"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\n",
    "# Parameters for grid search\n",
    "param_grid = {'xgb_classifier__base_score': [0.5],\n",
    "              'xgb_classifier__booster': ['gbtree'],\n",
    "              'xgb_classifier__colsample_bylevel': [1],\n",
    "              'xgb_classifier__colsample_bytree': [1],\n",
    "              'xgb_classifier__gamma': [0],\n",
    "              'xgb_classifier__learning_rate': [0.1, 0.5, 0.9],\n",
    "              'xgb_classifier__max_delta_step': [0],\n",
    "              'xgb_classifier__max_depth': [4],\n",
    "              'xgb_classifier__min_child_weight': [1],\n",
    "              'xgb_classifier__missing': [None],\n",
    "              'xgb_classifier__n_estimators': [2, 50],\n",
    "              'xgb_classifier__nthread': [1],\n",
    "              'xgb_classifier__objective': ['binary:logistic'],\n",
    "              'xgb_classifier__reg_alpha': [0],\n",
    "              'xgb_classifier__reg_lambda': [1],\n",
    "              'xgb_classifier__scale_pos_weight': [1],\n",
    "              'xgb_classifier__seed': [0],\n",
    "              'xgb_classifier__silent': [1],\n",
    "              'xgb_classifier__subsample': [0.7]}\n",
    "\n",
    "grid = GridSearchCV(pipe, cv=3, param_grid=param_grid, scoring=\"roc_auc\")\n",
    "grid.fit(X_tr, Y_tr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## prediction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>customerID</th>\n",
       "      <th>gender</th>\n",
       "      <th>SeniorCitizen</th>\n",
       "      <th>Partner</th>\n",
       "      <th>Dependents</th>\n",
       "      <th>tenure</th>\n",
       "      <th>PhoneService</th>\n",
       "      <th>MultipleLines</th>\n",
       "      <th>InternetService</th>\n",
       "      <th>OnlineSecurity</th>\n",
       "      <th>...</th>\n",
       "      <th>DeviceProtection</th>\n",
       "      <th>TechSupport</th>\n",
       "      <th>StreamingTV</th>\n",
       "      <th>StreamingMovies</th>\n",
       "      <th>Contract</th>\n",
       "      <th>PaperlessBilling</th>\n",
       "      <th>PaymentMethod</th>\n",
       "      <th>MonthlyCharges</th>\n",
       "      <th>TotalCharges</th>\n",
       "      <th>Churn</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>5216</th>\n",
       "      <td>3034-ZBEQN</td>\n",
       "      <td>Female</td>\n",
       "      <td>0</td>\n",
       "      <td>Yes</td>\n",
       "      <td>No</td>\n",
       "      <td>48</td>\n",
       "      <td>No</td>\n",
       "      <td>No phone service</td>\n",
       "      <td>DSL</td>\n",
       "      <td>No</td>\n",
       "      <td>...</td>\n",
       "      <td>Yes</td>\n",
       "      <td>No</td>\n",
       "      <td>No</td>\n",
       "      <td>No</td>\n",
       "      <td>One year</td>\n",
       "      <td>No</td>\n",
       "      <td>Mailed check</td>\n",
       "      <td>34.70</td>\n",
       "      <td>1604.50</td>\n",
       "      <td>Yes</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3168</th>\n",
       "      <td>4893-GYUJU</td>\n",
       "      <td>Female</td>\n",
       "      <td>0</td>\n",
       "      <td>No</td>\n",
       "      <td>No</td>\n",
       "      <td>50</td>\n",
       "      <td>Yes</td>\n",
       "      <td>No</td>\n",
       "      <td>No</td>\n",
       "      <td>No internet service</td>\n",
       "      <td>...</td>\n",
       "      <td>No internet service</td>\n",
       "      <td>No internet service</td>\n",
       "      <td>No internet service</td>\n",
       "      <td>No internet service</td>\n",
       "      <td>One year</td>\n",
       "      <td>Yes</td>\n",
       "      <td>Mailed check</td>\n",
       "      <td>19.35</td>\n",
       "      <td>1033.00</td>\n",
       "      <td>No</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4906</th>\n",
       "      <td>8639-NHQEI</td>\n",
       "      <td>Female</td>\n",
       "      <td>0</td>\n",
       "      <td>Yes</td>\n",
       "      <td>Yes</td>\n",
       "      <td>72</td>\n",
       "      <td>Yes</td>\n",
       "      <td>Yes</td>\n",
       "      <td>Fiber optic</td>\n",
       "      <td>Yes</td>\n",
       "      <td>...</td>\n",
       "      <td>No</td>\n",
       "      <td>No</td>\n",
       "      <td>Yes</td>\n",
       "      <td>No</td>\n",
       "      <td>Two year</td>\n",
       "      <td>Yes</td>\n",
       "      <td>Bank transfer (automatic)</td>\n",
       "      <td>95.90</td>\n",
       "      <td>6954.15</td>\n",
       "      <td>No</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>3 rows × 21 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "      customerID  gender  SeniorCitizen Partner Dependents  tenure  \\\n",
       "5216  3034-ZBEQN  Female              0     Yes         No      48   \n",
       "3168  4893-GYUJU  Female              0      No         No      50   \n",
       "4906  8639-NHQEI  Female              0     Yes        Yes      72   \n",
       "\n",
       "     PhoneService     MultipleLines InternetService       OnlineSecurity  \\\n",
       "5216           No  No phone service             DSL                   No   \n",
       "3168          Yes                No              No  No internet service   \n",
       "4906          Yes               Yes     Fiber optic                  Yes   \n",
       "\n",
       "      ...       DeviceProtection          TechSupport          StreamingTV  \\\n",
       "5216  ...                    Yes                   No                   No   \n",
       "3168  ...    No internet service  No internet service  No internet service   \n",
       "4906  ...                     No                   No                  Yes   \n",
       "\n",
       "          StreamingMovies  Contract PaperlessBilling  \\\n",
       "5216                   No  One year               No   \n",
       "3168  No internet service  One year              Yes   \n",
       "4906                   No  Two year              Yes   \n",
       "\n",
       "                  PaymentMethod MonthlyCharges  TotalCharges  Churn  \n",
       "5216               Mailed check          34.70       1604.50    Yes  \n",
       "3168               Mailed check          19.35       1033.00     No  \n",
       "4906  Bank transfer (automatic)          95.90       6954.15     No  \n",
       "\n",
       "[3 rows x 21 columns]"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "X_ts.head(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "prediction = grid.best_estimator_.predict_proba(X_ts)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[0.93262005, 0.06737993],\n",
       "       [0.9616315 , 0.03836851],\n",
       "       [0.97028416, 0.02971583],\n",
       "       ...,\n",
       "       [0.62810165, 0.37189835],\n",
       "       [0.39844203, 0.60155797],\n",
       "       [0.3066448 , 0.6933552 ]], dtype=float32)"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "prediction"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
