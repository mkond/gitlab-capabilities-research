models package
==============

Subpackages
-----------

.. toctree::

    models.xgboost_learning

Submodules
----------

models.ml\_models module
------------------------

.. automodule:: models.ml_models
    :members:
    :undoc-members:
    :show-inheritance:

models.model\_container module
------------------------------

.. automodule:: models.model_container
    :members:
    :undoc-members:
    :show-inheritance:

models.model\_creator module
----------------------------

.. automodule:: models.model_creator
    :members:
    :undoc-members:
    :show-inheritance:

models.model\_shap\_saver module
--------------------------------

.. automodule:: models.model_shap_saver
    :members:
    :undoc-members:
    :show-inheritance:

models.nn\_models module
------------------------

.. automodule:: models.nn_models
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: models
    :members:
    :undoc-members:
    :show-inheritance:
