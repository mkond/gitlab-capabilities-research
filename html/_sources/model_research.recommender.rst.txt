model\_research.recommender package
===================================

Submodules
----------

model\_research.recommender.data\_grid\_builder module
------------------------------------------------------

.. automodule:: model_research.recommender.data_grid_builder
    :members:
    :undoc-members:
    :show-inheritance:

model\_research.recommender.group\_recommender module
-----------------------------------------------------

.. automodule:: model_research.recommender.group_recommender
    :members:
    :undoc-members:
    :show-inheritance:

model\_research.recommender.recommender module
----------------------------------------------

.. automodule:: model_research.recommender.recommender
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: model_research.recommender
    :members:
    :undoc-members:
    :show-inheritance:
