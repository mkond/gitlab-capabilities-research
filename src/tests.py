import unittest

from src.multiply import multiply
from src.sum_user import mysum


class TestUserMethods(unittest.TestCase):
    def test_sum(self):
        self.assertTrue(mysum(2, 2) == 4, "mysum func error")

    def test_multiply(self):
        self.assertTrue(multiply(2, 3) == 6, "Multiply func error")
